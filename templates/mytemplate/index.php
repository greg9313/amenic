<?php defined( '_JEXEC' ) or die( 'Restricted access' );?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" >
	<head>
		<jdoc:include type="head" />
		<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/system.css" type="text/css" />
		<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/general.css" type="text/css" />
		<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/template.css" type="text/css" />
	</head>
	<body>
		<header>
			<img src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/img/logo.png" class="logo" />
			<jdoc:include type="modules" name="top" /> 
		</header>
		<content>
			<jdoc:include tyoe="modules" name="left" />
			<jdoc:include type="component" />
			<jdoc:include tyoe="modules" name="right" />
		</content>
		<footer>
			<jdoc:include type="modules" name="footer" />
		</footer>
	</body>
</html>