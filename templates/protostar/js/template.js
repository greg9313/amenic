/**
 * @package     Joomla.Site
 * @subpackage  Templates.protostar
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @since       3.2
 */

// function cssEdit() {
// 		// Variable x = récupère l'élèment ayant comme ID "demo" et change la taille de la police ainsi que la couleur du texte
	
// 	    var x = document.getElementById("demo");
// 	    x.style.fontSize = "25px";           
// 	    x.style.color = "red"; 
// 	}
// function cssEditBack() {
// 	x.style.fontSize = "13px";
// 	x.style.color
// }
	
(function($)
{	
	// Fonction pour le jeu : voiture
	$(function() {
		var ok = 1;
		function deplace()
		{
			$('#vr').animate({top: '-=600'}, 2500, 'linear', function(){
				var vrX = Math.floor(Math.random()*194)+70;
				var vrY = 400;
				$('#vr').css('top',vrY);
				$('#vr').css('left',vrX);
				ok = 1;
			});
			$('.fond').animate({top: '-=360'}, 1000, 'linear', function(){
				$('.fond').css('top',0);
				deplace();
			});
		};

		$(document).keydown(function(e){
		if (e.which == 39)
		{
			vjX = parseInt($('#voiture').css('left'));
			if (vjX < 280)
			$('#voiture').css('left', vjX+30);
		}
		if (e.which == 37)
		{
			vjX = parseInt($('#voiture').css('left'));
			if (vjX > 70)
			$('#voiture').css('left', vjX-30);
		}
		});

		function collision()
		{
			vjX = parseInt($('#voiture').css('left'));
			vrX = parseInt($('#vr').css('left'));
			vjY = 10;
			vrY = parseInt($('#vr').css('top'));
			if (((vrX > vjX) && (vrX < (vjX+66)) && (vrY > vjY) && (vrY < (vjY+150)) && (ok == 1)) 
			|| ((vjX > vrX) && (vjX < (vrX+66)) && (vrY > vjY) && (vrY < (vjY+150)) && (ok == 1)))
			{
				collision = parseInt($('#info').text()) + 1;
				$('#info').text(collision);
				ok = 0;
			}
		}
		deplace();
		setInterval(collision, 20);
	});	
	/**********************************************************************************************************/







	// Fonction pour le jeu : Vaisseau
	$(function(){
		var stopDetection = 0;

		//fonction pour gérer les evenements lié à la pression d'une touche (pav.num)
		$(document).keydown(function(e){
			// Vers la droite (touche 6)
			if (e.which == 39)
			{
				//Enregistre les coordonnées de la position left 
				posX = parseInt($('#soucoupe').css('left'));
				// si la valeur de left est inférieur à 470...
				if (posX < 470)
				//... on modifie la valeur de left de "soucoupe" par sa valeur + 30
				$('#soucoupe').css('left', posX+30);
			}
			// Vers la gauche (touche 4)
			if (e.which == 37) 
			{
				posX = parseInt($('#soucoupe').css('left'));
				// si la valeur de left est supérieur à 20...
				if (posX > 20)
				//... on modifie la valeur de left de "soucoupe" par sa valeur - 30
				$('#soucoupe').css('left', posX-30);
			}   
			// Vers le bas (touche 2)
			if (e.which == 40) 
			{
				//Enregistre les coordonnées de la position top 
				posY = parseInt($('#soucoupe').css('top'));
				// si la valeur de top est inférieur à 230...
				if (posY < 230)
				//... on modifie la valeur de top de "soucoupe" par sa valeur + 30
				$('#soucoupe').css('top', posY+30);
			} 
			// Vers le haut (touche 8)
			if (e.which == 38) 
			{
				posY = parseInt($('#soucoupe').css('top'));
				// si la valeur de top est supérieur à 20...
				if (posY > 20)
				//... on modifie la valeur de top de "soucoupe" par sa valeur - 30
				$('#soucoupe').css('top', posY-30);
			} 
			// Vers le haut et la gauche (touche 7)
			if (e.which == 36) 
			{
				//Enregistre les coordonnées de la position left et top
				posX = parseInt($('#soucoupe').css('left'));
				posY = parseInt($('#soucoupe').css('top'));
				// si la valeur de left est supérieur à 20 et top supérieur à 20...
				if ((posY > 20) && (posX > 20))
				//... on modifie la valeur de top et left de "soucoupe" par leur valeur - 30
				$('#soucoupe').css('left', posX-30).css('top', posY-30);
			} 
			// Vers le haut et la droite (touche 9)
			if (e.which == 33) 
			{
				posX = parseInt($('#soucoupe').css('left'));
				posY = parseInt($('#soucoupe').css('top'));
				if ((posY > 20) && (posX < 470))
				$('#soucoupe').css('left', posX+30).css('top', posY-30);
			} 
			// Vers le bas et la gauche (touche 1)
			if (e.which == 35) 
			{
				posX = parseInt($('#soucoupe').css('left'));
				posY = parseInt($('#soucoupe').css('top'));
				if ((posX > 20) && (posY < 230))
				$('#soucoupe').css('left', posX-30).css('top', posY+30);
			}  
			// Vers le bas et la droite (touche 3)
			if (e.which == 34) 
			{
				posX = parseInt($('#soucoupe').css('left'));
				posY = parseInt($('#soucoupe').css('top'));
				if ((posY < 230) && (posX < 470))
				$('#soucoupe').css('left', posX+30).css('top', posY+30);
			}
		});

		function afficheElements()
		{
			//Initialise stopDetection à 0 lorsqu'un nouvel élèment apparait
			stopDetection = 0;
			//Enregistre dans la variable elemX un chiffre aléatoire compris entre 0 et 309
			var elemX = Math.floor(Math.random()*310);
			//Enregistre dans la variable elemY un chiffre aléatoire compris entre 0 et 250
			var elemY = Math.floor(Math.random()*251);
			//Enregistre dans la variable elemType un chiffre aléatoire compris entre 0 et 1
			var elemType = Math.floor(Math.random()*2);

			

			//Si la valeur de elemType = 0, on affiche l'image ayant l'id "bon", sinon, on affiche l'autre
			if (elemType == 0)
			{
				//Donne une position aléatoire a l'élèment id=bon et l'affiche
				$('#bon').css('top',elemY).css('left',elemX);
				$('#bon').show();
				$('#mauvais').css('display','none');
			}
			else
			{
				//Donne une position aléatoire a l'élèment id=mauvais et l'affiche
				$('#mauvais').css('top',elemY).css('left',elemX);
				$('#mauvais').show();
				$('#bon').css('display','none');
			}

		}
		function collisions()
		{
			//Enregistre les valeurs des positions left et top de l'id=soucoupe
			posX = parseInt($('#soucoupe').css('left'));
			posY = parseInt($('#soucoupe').css('top'));
			//Vérifie la condition de la fonction afficheElements, si id=bon est en display none (2e condition de afficheElements)...
			if ($('#bon').css('display') == 'none')
			{
				//La variable elemType prend la valeur mauvais
				elemType = 'mauvais';
				//Enregistre les valeurs des positions left et top de l'id=mauvais
				elemX = parseInt($('#mauvais').css('left'));
				elemY = parseInt($('#mauvais').css('top'));
			}
			else 
			{
				//La variable elemType prend la valeur bon
				elemType = 'bon';
				//Enregistre les valeurs des positions left et top de l'id=bon
				elemX = parseInt($('#bon').css('left'));
				elemY = parseInt($('#bon').css('top'));
			}
			//Si la position left d'elemX est supérieur à la position left-20 de la soucoupe ET est inférieur à la position left+125-50+5  ET
			//si la position top d'elemY est supérieur à la position top-20 de la soucoupe ET est inférieur à la position  top+177-116+5 ET
			//si stopDetection = 0...
			//posX+125 = largeur de la soucoupe    -50 = largeur des élèments à collecter      +/-15 = marge 
			//posY+177 = hauteur de la soucoupe    -116 = hauteur des élèments à collecter     +/-15 = marge
			// if ((elemX>posX) && (elemX<(posX+15)) && (elemY>posY) && (elemY<(posY+15)) && (stopDetection == 0))
			if ((elemX>(posX-29)) && (elemX<(posX+29)) && (elemY>(posY-29)) && (elemY<(posY+29)) && (stopDetection == 0))
			{
				stopDetection = 1;
				//Vérifie avec quel élèment à eu lieu la collision, s'il s'agit de l'élèment id=bon...
				if (elemType=='bon')
				{
					//récupère dans la variable nbBon la valeur du bloc ayant l'id info1 et l'incrémente de 1
					var nbBon = parseInt($('#info1').text())+1;
					//modifie le text de info1 pour affiché la nouvelle valeur incrémentée
					$('#info1').text(nbBon);
					//récupère dans la variable score la valeur du bloc ayant l'id info3 et l'incrémente de 5
					var score = parseInt($('#info3').text())+5;
					//modifie le text de info3 pour affiché la nouvelle valeur incrémentée
					$('#info3').text(score);
					//Fait disparaitre l'élèment id=bon en appliquant le style display: none
					$('#bon').css('display', 'none');
				}
				else // s'il ne s'agit pas de l'élèment id=bon...
				{
					//récupère dans la variable nbMauvais la valeur du bloc ayant l'id info2 et l'incrémente de 1
					var nbMauvais = parseInt($('#info2').text())+1;
					//modifie le text de info2 pour affiché la nouvelle valeur incrémentée
					$('#info2').text(nbMauvais);
					//récupère dans la variable score la valeur du bloc ayant l'id info3 et l'incrémente de -5
					var score = parseInt($('#info3').text())-5;
					//modifie le text de info3 pour affiché la nouvelle valeur incrémentée
					$('#info3').text(score);
					//Fait disparaitre l'élèment id=mauvais en appliquant le style display: none
					$('#mauvais').css('display', 'none');
				}
			}
		}
		//appel de façon répétitive la fonction afficheElements, 2 secondes
		setInterval(afficheElements, 3500);
		//Appel de façon répétitive la fonction collisions, 0.2 seconde
		setInterval(collisions, 100);
		/**********************************************************************************************************/
	});










	$(document).ready(function()
	{
		/**********************************************************************************************************
		// récupère la valeur du champs ayant l'id "test" et l'affiche
		alert("Value: " + $("#test").val());

		//récupère la valeur href du lien ayant l'id "test"
		alert($("#test").attr("href"));

		//récupère la valeur du paramètre width du bloc ayant l'id "div1"
		$("#div1").width();
		$("#div1").innerWidth();  --> width + les padding (si padding = "5", +5 a gauche, +5 a droite = width +10)
		$("#div1").outerWidth(); --> width + padding + border

		//récupère la valeur du paramètre height du bloc ayant l'id "div1"
		$("#div1").height();
		$("#div1").innerHeight();  --> height + les padding (si padding = "5", +5 en bas, +5 en haut = height +10)
		$("#div1").oouterHeight(); --> height + padding + border

		********************************************
		********************************************

		// EXEMPLE AVEC : <div> <ul><li><span> </span></li></ul>       <p><span> </span></p> </div>
		//vise les parent direct de span (li et p)
		$("span").parent().css[...];
		//vise les enfants direct de div (ul et p)
		$("div").children().css[...];

		//vise le parent p de span
		$("span").parent("p").css[...];
		//vise l'enfant ul de div
		$("div").children("ul").css[...];

		//Vise les parents de span (li, ul, div, body)
		$("span").parents().css[...];   --> children = l'enfant
		// vise le parent ul de span
		$("span").parent(ul).css[...];

		//vise l'enfant li de div
		$("div").find("li").css[...];
		//Vise tout les élèments enfant de div
		$("div").find("*").css[...];

		//vise les parents de span qui sont les enfants de div ==> ul, li
		$("span").parentsUntil("div").css[...];

		//Vise les blocs qui sont aussi enfant du parent de ul (ici, vise p)
		$("ul").siblings();
		//Vise les blocs p qui sont aussi enfant du parent de ul
		$("ul").siblings("p")

		//Vise le prochain bloc qui est aussi enfant du parent de ul (ici, p)
		$("ul").next();
		$("ul").nextAll() // visera tout les bloc après ul 

		//Vise les blocs après ul et avant p, qui sont aussi enfant des parents de ul et p (qui sont aux meme niveau eux-même), avec l'exemple  : <div><ul><li><span></span></li></ul> <h2></h2> <p><span></span></p></div>, h2 sera visé
		$("ul").nextUntil("p");

		NEXT peut etre remplacé par PREV


		//Vise le premier p de la première div
		$("div p").first()  --> avec last, vise le dernier p de la dernière div
		
		//Vise le 3e bloc p,  eq(x) -> vise l element d'index 2 (le premier paragraphe aura l index 0)
		$("p").eq(2)

		//Vise les bloc p ayant la classe intro
		$("p").filter(".intro")
		
		//Vise les blocs p n'ayant pas la classe intro
		$("p").not(".intro")

		//Charge le fichier text.txt pour afficher son contenu dans le bloc ayant l'id div1, lors du clic sur le bouton, et lui donne l'id "id"
		$("button").click(function(){
	        $("#div1").load("text.txt #id");

	    //Remplace le texte de h1 par le text "abc"
	    jQuery("h1").text("abc");

		//Math.random() pour tirer des nombres aléatoires. 
		var elemX = Math.floor(Math.random()*500)+20; (nombre entre 0 et 519

		//Mémoriser valeur position left dans la variable posX
		posX = parseInt($('#soucoupe').css('left'));)
	    });
		**********************************************************************************************************/










		//boutton pour cacher / afficher le header
		$("#btnheader").click(function(){
			$("header").toggle(1000);
		});
		//boutton pour cacher / afficher le menu
		$("#btnnav").click(function(){
			$("nav ul.nav").toggle(1000);
		});
		//boutton pour cacher / afficher le champs de recherche
		$("#btnsrch").click(function(){
			$("nav .search").toggle(1000);
		});
		//boutton pour cacher / afficher le menu de gauche
		$("#btnnavleft").click(function(){
			$("#sidebar").toggle(1000);
		});
		//bouton pour tout cacher / afficher
		var btntoggle = true;
		$("#btnall").click(function(){
			//Condition qui vérifie que tout se ferme ou que tout s'ouvre ensemble
			if (btntoggle){
				$("header").hide(1500);
				$("nav ul.nav").hide(3000);
				$("nav .search").hide(2000);
				$("#sidebar").hide(2000);
			}else{
				$("header").show(1500);
				$("nav ul.nav").show(3000);
				$("nav .search").show(2000);
				$("#sidebar").show(2000);
			}
			btntoggle = !btntoggle;
		});
		//boutton pour cacher le site
		$("#explode").click(function(){
			$( ".body" ).hide(1000);
			$( ".footer" ).hide(1000);
			$("#btnrefresh").css({"display": "block"});
		});
		//boutton pour réafficher le site
		$("#btnrefresh").click(function(){
			$( ".body" ).show(1000);
			$( ".footer" ).show(1000);
			$("#btnrefresh").css({"display": "none"});
		});
		//boutton pour faire planter le site
		$("#btnbug").click(function(){
			$("body").toggle("explode");
		});
		// Affiche le text contenu dans la div panel après avoir cliquer sur la div flip
		$("#flip").click(function(){
			$("#panel").slideToggle("slow");
		});
		//boutton supprimant les bouttons
		$("#removebtn").click(function(){
        	$("button").remove();
        	//empty permet de vider la valeur, les bouttons n'auront plus de text à l'intérieur
        	//$("button").empty();
    	});
		/**********************************************************************************************************/








		// Changement du background-color et du border-color lorsque les champs d'un formulaire sont focus
		$("input").focus(function(){
			$(this).css({"background-color": "#ffeeee", "border-color": "red"});
		});
		$("input").blur(function(){
			$(this).css({"background-color": "#ffffff", "border-color": "#cccccc"});
		});
		/**********************************************************************************************************/








		// Changement du CSS du texte ayant l'ID 'demo' lors du clique sur le boutton 'but_1'
		$("#but_1").click(function(){
        	$("#demo").toggleClass("statecls");
    	});
		/**********************************************************************************************************/








		

		// changement avec animation du CSS d un bloc de texte ayant l'id 'effect' lors du clique sur le boutton 'button'
		var stats = true;
		$('#button').click(function() {
		  	if(stats) {
		    	$('#effect').animate({
			    	backgroundColor : "#bb0022",
			    	color: "yellow",
			    	width: 500
		    	}, 1000);
		  	} else {
			    $('#effect').animate({
				    backgroundColor : "#fff",
				    color: "#222",
				    width: 240
			    }, 1000);
		  	}
		 	stats = !stats;
		});
		/**********************************************************************************************************/	







	
  
	    // Autocomplete Search
	    $( "#mod-search-searchword" ).autocomplete({
	    	source: availableTags
	    });
	    $( "#mod-search-searchword" ).on( "autocompleteselect", function() { $('#searchform').submit();} );
		$('*[rel=tooltip]').tooltip()
		// Turn radios into btn-group
		$('.radio.btn-group label').addClass('btn');
		$(".btn-group label:not(.active)").click(function(){
			var label = $(this);
			var input = $('#' + label.attr('for'));
			if (!input.prop('checked')) {
				label.closest('.btn-group').find("label").removeClass('active btn-success btn-danger btn-primary');
				if (input.val() == '') {
					label.addClass('active btn-primary');
				} else if (input.val() == 0) {
					label.addClass('active btn-danger');
				} else {
					label.addClass('active btn-success');
				}
				input.prop('checked', true);
			}
		});
		$(".btn-group input[checked=checked]").each(function(){
			if ($(this).val() == '') {
				$("label[for=" + $(this).attr('id') + "]").addClass('active btn-primary');
			} else if ($(this).val() == 0) {
				$("label[for=" + $(this).attr('id') + "]").addClass('active btn-danger');
			} else {
				$("label[for=" + $(this).attr('id') + "]").addClass('active btn-success');
			}
		});
		// Effectue la requete de recherche lors du clic sur un choix de l'autocomplete
		$('#ui-id-1').click(function() {
			$('#searchform').submit();
		});
		/**********************************************************************************************************/









		// Effet d'animation sur bloc de text 
		function runEffect() {
			//                   valeur sélectionnée dans la liste déroulante 'effectTypes'
			var selectedEffect = $("#effectTypes").val();
			var options = {};

			// Certains effets sont pré-programmé, pour 'scale', 'transfer' et 'size', il faut les programmés
			if (selectedEffect === "scale") {
				options = {percent: 0};
			} else if (selectedEffect === "transfer") {
				options = {to: "#selectbutton", className: "ui-effects-transfer"};
			} else if (selectedEffect === "size") {
				options = {to: {width: 200, height: 60}};
			}
			$("#effet").effect(selectedEffect, options, 500, callback);
			// Pour cacher le bloc de text avec un effet, remplacer le '.effect' par '.hide' :
			// $("#effet").hide(selectedEffect, options, 500, callback);
			// Pour afficher le bloc de text avec un effet (non-visible de base), remplacer le '.effect' par '.show' :
			// $("#effet").show(selectedEffect, options, 500, callback);
			// Pour cacher/afficher le bloc de text avec un effet lors du clic sur le bouton, remplacer '.effect' par '.toggle' :
			//$("#effet").toggle( selectedEffect, options, 500 );
		};
		// fait revenir à l'état initial 1 seconde après l'effet (à enlever pour l'utilisation de .toggle)
		function callback() {
			setTimeout(function() {
				$( "#effet" ).removeAttr( "style" ).hide().fadeIn();
				//Si le .effect/.hide est remplacé par le .show
				//$("#effet:visible").removeAttr("style").fadeOut();
			}, 1000 );
		};
		// lance l'effet après clic sur le boutton
		$("#selectbutton").click(function() {
			runEffect();
			return false;
		});
		//pour le .show
 		//$( "#effets" ).hide();
		/**********************************************************************************************************/










 		//QUESTIONNAIRE
 		//cache les réponses
 		$('.reponse').hide();
 		//Au survol du lien par la souris
		$('#answer').click(
			function() { 
				$('.reponse').show().delay(3000).fadeOut();
				// Vérifie si les bonnes réponses ont été cochées
				// question 1
				if ($(':radio[id="r3"]:checked').val()) {
					$('#img1').attr('src', 'http://user.oc-static.com/files/386001_387000/386923.png'); 
					$('#reponse1').css('color', 'green');
				}  
				else {
					$('#img1').attr('src', 'http://user.oc-static.com/files/386001_387000/386924.png');
					$('#reponse1').css('color', 'red');
				} 
				// question 2
				if ($(':radio[id="r5"]:checked').val()) {
					$('#img2').attr('src', 'http://user.oc-static.com/files/386001_387000/386923.png');
					$('#reponse2').css('color', 'green');
				}
				else {
					$('#img2').attr('src', 'http://user.oc-static.com/files/386001_387000/386924.png');
					$('#reponse2').css('color', 'red');
				} 
				// question 3
				if ($(':radio[id="r7"]:checked').val()) {
					$('#img3').attr('src', 'http://user.oc-static.com/files/386001_387000/386923.png'); 
					$('#reponse3').css('color', 'green');
				}
				else {
					$('#img3').attr('src', 'http://user.oc-static.com/files/386001_387000/386924.png');
					$('#reponse3').css('color', 'red');
				}
			}
		);	
		/**********************************************************************************************************/			
	});
})(jQuery);